/*
 * @fullReview  Mohan AMILINENI  16/11/2018  Initial Version 
 * 
 */
package com.auto_general.todoservices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.boot.builder.SpringApplicationBuilder;
//import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

/**
 * Spring Boot Application to manage Auto and General Assignments
 * 
 * @author Mohan AMILINENI
 *
 */
@SpringBootApplication
//public class AutoGeneralTodoApplication extends SpringBootServletInitializer {
public class AutoGeneralTodoApplication{

	public static void main(String[] args) {
		SpringApplication.run(AutoGeneralTodoApplication.class, args);
	}
	
/*    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(AutoGeneralTodoApplication.class);
    }*/
}