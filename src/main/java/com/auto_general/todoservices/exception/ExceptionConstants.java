/*
 * @fullReview  Mohan AMILINENI  16/11/2018  Initial Version 
 * 
 */
package com.auto_general.todoservices.exception;

/**
 * This class manages all the exception related constants for todo application
 * @author Mohan AMILINENI
 *
 */
public class ExceptionConstants {
	
	public static final String PARAMS 		    = "params";
	public static final String VALIDATION_ERROR = "ValidationError";
	public static final String NOT_FOUND_ERROR  = "NotFoundError";

}
